//
//  ViewController.h
//  20160426_iOSDay6_TabBox
//
//  Created by ChenSean on 4/26/16.
//  Copyright © 2016 ChenSean. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Global.h"

@interface ViewController : UIViewController <UIPickerViewDataSource, UIPickerViewDelegate>
{
    NSMutableArray *list1, *list2;
    NSString *continent, *country;
}
@property (weak, nonatomic) IBOutlet UIPickerView *myPickerView;


@end

