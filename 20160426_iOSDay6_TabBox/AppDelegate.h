//
//  AppDelegate.h
//  20160426_iOSDay6_TabBox
//
//  Created by ChenSean on 4/26/16.
//  Copyright © 2016 ChenSean. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

//宣告 global 變數的地方
@property NSString *continent, *country;


@end

