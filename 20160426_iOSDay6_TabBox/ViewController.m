//
//  ViewController.m
//  20160426_iOSDay6_TabBox
//
//  Created by ChenSean on 4/26/16.
//  Copyright © 2016 ChenSean. All rights reserved.
//

#import "ViewController.h"
#import "AppDelegate.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    list1 = [NSMutableArray new];
    list2 = [NSMutableArray new];
    
    [list1 addObject:@"歐洲"];
    [list1 addObject:@"亞洲"];
    [list1 addObject:@"美洲"];
    
    [list2 addObject:@"荷蘭"];
    [list2 addObject:@"丹麥"];
    [list2 addObject:@"日本"];
    [list2 addObject:@"美國"];
    [list2 addObject:@"加拿大"];
    
    continent = [list1 objectAtIndex:0];
}


- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 2;
}

// 設定 Picker View 上面每一個滾筒要呈現幾筆資料
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if (component == 0) {
        return [list1 count];
    }
    else if (component == 1)
    {
        return [list2 count];
    }
    
    return 0;
}

// 實際每一個滾筒上要呈現的資料內容
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    if (component == 0)
    {
        return [list1 objectAtIndex:row];
    }
    else if (component == 1)
    {
        return [list2 objectAtIndex:row];
    }
    
    return nil;
}

// 取得使用者所選取的項目
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if (component == 0) {
        continent = [list1 objectAtIndex:row];
//        NSLog(@"使用者在五大洲選擇了%@", [list1 objectAtIndex:row]);
    } else if (component == 1){
        country = [list2 objectAtIndex:row];
//        NSLog(@"使用者在國家選擇了%@", [list2 objectAtIndex:row]);
        
        NSLog(@"使用者選擇了%@ %@", continent, country);
        
        // 將 app delegate 強迫轉型 book:25-1(呼叫 AppDelegate，會常用)
        AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
        
        // 強轉型，指定 Controller 類型
        UITabBarController *tabBar =  (UITabBarController*)app.window.rootViewController;
        [tabBar setSelectedIndex:1];
        
        // 方法一：
//        UINavigationController *naviController = tabBar.viewControllers[1];
//        UIViewController *vc = naviController.viewControllers[0];
//        
//        for (UIView *p in vc.view.subviews) {
//            if ([p isKindOfClass:[UILabel class]]) {
//                if (p.tag == 100) {
//                    ((UILabel*)p).text = continent;
//                } else if (p.tag == 200){
//                    ((UILabel*)p).text = country;
//                }
//            }
//        }
        
        // 方法二：
//        app.continent = continent;
//        app.country = country;
        
        // 方法三：
        NSUserDefaults *user = [NSUserDefaults standardUserDefaults];
        [user setObject:continent forKey: MYKEY_CONTINENT];
        [user setObject:country forKey: MYKEY_COUNTRY];
        // 讓變數立馬存到 NUserDefaults setObject 去，不要等作業系統自動存，會造成取得不同步
        [user synchronize];
        
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
