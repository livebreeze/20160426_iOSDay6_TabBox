//
//  TravelVC.m
//  20160426_iOSDay6_TabBox
//
//  Created by ChenSean on 4/28/16.
//  Copyright © 2016 ChenSean. All rights reserved.
//

#import "TravelVC.h"
#import "AppDelegate.h"

@interface TravelVC ()
@property (weak, nonatomic) IBOutlet UILabel *lbl_Continent;
@property (weak, nonatomic) IBOutlet UILabel *lbl_Country;
@end

@implementation TravelVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    // load 只會被呼叫一次，所以以下 code 應該要放到 DidAppear 去
    //    AppDelegate *app = [UIApplication sharedApplication].delegate;
    //    self.lbl_Continent.text = app.continent;
    //    self.lbl_Country.text = app.country;
    }

- (void)viewDidAppear:(BOOL)animated{
//    AppDelegate *app = [UIApplication sharedApplication].delegate;
//    self.lbl_Continent.text = app.continent;
//    self.lbl_Country.text = app.country;
    
    NSUserDefaults *user = [NSUserDefaults standardUserDefaults];
    self.lbl_Continent.text = [user objectForKey: MYKEY_CONTINENT];
    self.lbl_Country.text = [user objectForKey: MYKEY_COUNTRY];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
@end
